const fetch = require('node-fetch')

const accountName = "brandlive"
const environment = "vtexcommercestable.com.br"
const AppKey = "vtexappkey-brandlive-UJSZAY"
const AppToken = "QZJCSWNLFWPSENBMQJCMTLSJNDTBKQWEAARHTLXXGLPBDEWZUBOUOPVEHAPODMAPZLRDVUSFMFFHWQFJHTHJCCPLFNZSWJTULABSNSEZIJIJJOIQUFGJFRMWVGYHUAIC"

const url = `http://${accountName}.${environment}/api/checkout/pvt/configuration/orderForm`

const body = {
    "paymentConfiguration": 
	{
		"requiresAuthenticationForPreAuthorizedPaymentOption": false
	},
	"minimumQuantityAccumulatedForItems": 1,
	"decimalDigitsPrecision": 2,
	"minimumValueAccumulated": 0,
    "apps": 
	[
		{
			"id":"pickit",
			"fields":
			[
				"pickit-point-id"
			]
        }
    ]
} 

const fetch_options = {
    method: "POST",
    headers : {
        "Content-Type": "application/json",
        "X-VTEX-API-AppKey" : AppKey,
        "X-VTEX-API-AppToken" : AppToken
    },
    body : JSON.stringify(body)
}

async function run (){
    const resp = await fetch(url,fetch_options)
    const html = await resp.text()
    console.log(html)
}

run()