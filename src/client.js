(function(context){
    let testCount = 100
    function test(){
        if(!context.vtex || !context.vtexjs || !context.google || !context.google.maps){
            if(testCount){
                testCount --
                setTimeout(test,500)
            } else {
                debugger
                throw new Error ("context not loaded correctly")
            } 

        }else {
            loadPlugin(context.vtex,context.vtexjs,context.google)
        }
    }
    test()
    function loadPlugin(vtex,vtexjs,google){

        const pickitServiceFormData = new FormData();
        pickitServiceFormData.append("value",JSON.stringify({
            ApiKey : "apiKeyPublica",
            Metodo : "GetAllPuntos",
            Parametros : ""
        }))
        const pickitServiceUrl = 'https://coretest.pickitlabs.com/app.php/CallMethod'
        const pickitServiceOptions = {
            method: 'POST',
            cache: 'no-cache',
            body : pickitServiceFormData
        }
        const initMapOpts = {
            center : { lat : -34.589693,  lng : -58.518333},
            zoom: 4,
            mapTypeId: 'roadmap',
            mapTypeControl: false,
            streetViewControl: false,
            rotateControl: false,
            fullscreenControl: false
        }
        const iconimg = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAjCAYAAACOysqWAAABN2lDQ1BBZG9iZSBSR0IgKDE5OTgpAAAokZWPv0rDUBSHvxtFxaFWCOLgcCdRUGzVwYxJW4ogWKtDkq1JQ5ViEm6uf/oQjm4dXNx9AidHwUHxCXwDxamDQ4QMBYvf9J3fORzOAaNi152GUYbzWKt205Gu58vZF2aYAoBOmKV2q3UAECdxxBjf7wiA10277jTG+38yH6ZKAyNguxtlIYgK0L/SqQYxBMygn2oQD4CpTto1EE9AqZf7G1AKcv8ASsr1fBBfgNlzPR+MOcAMcl8BTB1da4Bakg7UWe9Uy6plWdLuJkEkjweZjs4zuR+HiUoT1dFRF8jvA2AxH2w3HblWtay99X/+PRHX82Vun0cIQCw9F1lBeKEuf1UYO5PrYsdwGQ7vYXpUZLs3cLcBC7dFtlqF8hY8Dn8AwMZP/fNTP8gAAAAJcEhZcwAACxMAAAsTAQCanBgAAAXraVRYdFhNTDpjb20uYWRvYmUueG1wAAAAAAA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjYtYzE0MiA3OS4xNjA5MjQsIDIwMTcvMDcvMTMtMDE6MDY6MzkgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpwaG90b3Nob3A9Imh0dHA6Ly9ucy5hZG9iZS5jb20vcGhvdG9zaG9wLzEuMC8iIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdEV2dD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlRXZlbnQjIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDQyAyMDE4IChXaW5kb3dzKSIgeG1wOkNyZWF0ZURhdGU9IjIwMTgtMDQtMjdUMDk6MzI6MzItMDM6MDAiIHhtcDpNb2RpZnlEYXRlPSIyMDE4LTA1LTA5VDE3OjA5OjQ5LTAzOjAwIiB4bXA6TWV0YWRhdGFEYXRlPSIyMDE4LTA1LTA5VDE3OjA5OjQ5LTAzOjAwIiBkYzpmb3JtYXQ9ImltYWdlL3BuZyIgcGhvdG9zaG9wOkNvbG9yTW9kZT0iMyIgcGhvdG9zaG9wOklDQ1Byb2ZpbGU9IkFkb2JlIFJHQiAoMTk5OCkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6Nzk2NDg3NGItODI3OC00OTQ3LWJiNjEtYzNhODMwY2EyZjM3IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOmJjOWU2MWMwLTNiNTAtYWQ0Yi05MjBlLTRkZjkzMzRmYTVjMyIgeG1wTU06T3JpZ2luYWxEb2N1bWVudElEPSJ4bXAuZGlkOmJjOWU2MWMwLTNiNTAtYWQ0Yi05MjBlLTRkZjkzMzRmYTVjMyI+IDx4bXBNTTpIaXN0b3J5PiA8cmRmOlNlcT4gPHJkZjpsaSBzdEV2dDphY3Rpb249ImNyZWF0ZWQiIHN0RXZ0Omluc3RhbmNlSUQ9InhtcC5paWQ6YmM5ZTYxYzAtM2I1MC1hZDRiLTkyMGUtNGRmOTMzNGZhNWMzIiBzdEV2dDp3aGVuPSIyMDE4LTA0LTI3VDA5OjMyOjMyLTAzOjAwIiBzdEV2dDpzb2Z0d2FyZUFnZW50PSJBZG9iZSBQaG90b3Nob3AgQ0MgMjAxOCAoV2luZG93cykiLz4gPHJkZjpsaSBzdEV2dDphY3Rpb249InNhdmVkIiBzdEV2dDppbnN0YW5jZUlEPSJ4bXAuaWlkOjc5NjQ4NzRiLTgyNzgtNDk0Ny1iYjYxLWMzYTgzMGNhMmYzNyIgc3RFdnQ6d2hlbj0iMjAxOC0wNS0wOVQxNzowOTo0OS0wMzowMCIgc3RFdnQ6c29mdHdhcmVBZ2VudD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTggKFdpbmRvd3MpIiBzdEV2dDpjaGFuZ2VkPSIvIi8+IDwvcmRmOlNlcT4gPC94bXBNTTpIaXN0b3J5PiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PjRkQ/cAAAZtSURBVEiJpZZ5cFXVHcc/59yb9/Je9uSFLERCICHSWiaW1KFCCWiRUqGVItpkCg6dSlLthlq7SMdK3XAvRSOIqDAaXChklCKLsSzBdBhMJURCYpMY5JE9Ly95L2+79/SPhIRAHmTa38yduef8fr/v52z3d65oO5jFiAmE0FDKABRCs6YrM/Bd098xVwU914KZBAglRI/U7F9Iq+OwkPYDyvQ3gwABKIPBlyHFywE6SjABw/eb0ICzGDOUqNnS0aKmoVmSQAjMgAvDc4aQ9ywI2a/b099Ei3oKZTSiQuEACiF0TPSbQ/3/2S7MYFpkykLsk+/BknQj0pIIyOFYM+gi2HMCT3Mp/vO7QQq3Fp1dggqWocxhyAhACExT/0mkaN6mWZOFNvk57Bl3MB7zdxzAVV2M4W0mIj7nQWWGnkap0QATfbHwNLxvm/A17PlH0PXEcYlfMGX003lkHkHXCSJic1YrZbwCCgkKhH6NbjhLU2bk8/Bb8/hpyYYxRXwm+NVIOxgMsPdgFS2tPQgtmqRv70GLmkSwv+EpRMQMUOggUKbvPqtFy1DXvExV7SYc2j7gzyNLEILNb+yiqqKchAQHBUsKGWit4YMDJ2g/8Sp5sxaz5sltZKakED/jZbqPL41XRt/vhbQVSSGtU40B51225FvocE8jyXWUqalpw+KnvvTw/R/cQWPZj7h32geU5LzFe48s4He/XMWySeUsvykL1fguW198AgBryiJs6XdieM8XCqnfIJXpm4NpJOhZd9PrjyElyUuU0UyTCw7XdLFiyXcoTHqX5x+YxfV5M7HG5vL2I9msWjqdPdVWIiNteCNTaavZSXV9KwD2ySUIKTFD7lukGegs0GxpYMsjKxXipszGdFazZW0Re1+4ndLlX/Gz5XNocWp0dPbyztE+KmsiePzONLo7PbR1uXlidR4SuLlgITWfO7EkzkKPmo7p754jVcibo9mzUSSiA8tKHqWObyA/LSO64xMmTMwgaEi8foP4KMkPZ2ocq+0DI8DfH85hwNNH+f5jFBcu4O6Z/dyz8jbchiBmYj7K8GdLlOmQlgSE0AGYnZfFk69XkPvrXQTmvcaKjT6Of1rD1HQbvd4gH5/yMjs/le72s9y+9hTxS3dQn/Ig27dsJSumkbPn6mg+D9bY6ShFnA4EwBx1HHMzHeRm3gbAN/Pzuf/+InZO+ZLD56fy16oIbj13kna3ZH7xBtYUFQAF7No7n7rPjvLs0ky+ngED9a0ICOF8P+Lt9kPfUmbIq8LZq+9VqvVFcer1hwpUznXzVZaGen5Dadh4pZTqPLZIOcupkkKP3m94GzB9X4X9SufcmE+v9XpOVlWysvheyo5Vs2plUdh409+G4akD3X5ESktShel3dfra9oVNSE+10CvSuC4jhOv0Ltx6MnFxsWHjA12fEPI2oVmTD0iE3iR02yZvyysoY2DMhGgBU/IXU9Fg5660N9lcksNfnt0RRl7had4IQv9YSFuFRBlotoyXgj0nW/ob1ocd1X0/LyK38B3+uH8aq2cP0Lh7NR8eqb0sztO0CX97Bbp94mZlBkKDBV5Ip2ZLftx9eh2+tj1hIWt/cSuPba/m89SH8PT7aaqpHL00ruO4a3+LtEb9E2HZAQxVU2UgI5M2aZHxu7urluFz7hwT0NJyltbaj7j2hiU4bvoDycmOEfGuw3RVfg8hlE+LTH9g8GZTiLaPsodCBEKzTDY8Lf8y/X0TorJ/Rcz0dUg9bliks8dD7akz2Gx2MrJyiIzRSIjw46lfj7tuHUK3okdN+pMy/I9eyLnkTgaEvkaF+p4zPK1Iexq2jBXYUheh2TKRVgdCs4DZA75zBDorcJ0pJdDfhG53IC2JhzCDC4BgeAAA8pCQ+lzD347h60FqAhmRirAO3nIq6ML0t6FCIYQ1Ft2WhlKGiTIWAgcvVtLH3k7zaWX652pWB9LqQAXdqFAfprd7cFRCR1oSEFFxCCFRZgDgtUvFrzADAMqAHw+FgRCjvQouqmFdwCzgi8vWIpw6sB4whtWUOfoZXSCfGUv8aoB/A29cwX/BGoCt4ZxXAgC8APRcJWYj0P6/AmoY3Itw9hkw9j/OOAEAfwO8YXzPXC15PIA64MUx+o9y5dmNGwCwBXBf0vcYw6fs/wfUA9suav8D+HA8ieMFALwEeIbeN483KUypGNNOA/uAdKB8vEn/BUV01098OM5RAAAAAElFTkSuQmCC"
        const style = `
        #pickit-modal{
            display:none;
            position:fixed;
            top:0;
            bottom:0;
            left:0;
            right:0;
            background-color:rgba(0,0,0,0.4);
            background-image:url('https://lightbox.pickit.net/bundles/public/img/loading.gif');
            background-repeat: no-repeat;
            background-position: center;
            z-index:9999;
        }

        #pickit-modal.open{
            display : block;
        }

        #pickit-modal .panel{
            position: fixed;
            left: 50%;
            top: 50%;
            transform: translate(-50%,-50%);
            background-color: #fff;
            border-radius: 5px;
            border: solid 1px rgba(0,0,0,.3);
            max-height: 100vh;
            height: 500px;
            overflow: hidden;
            max-width: 100vw;
            width: 1000px;
            box-shadow: 1em 1em 1em rgba(0,0,0,0.2);
            padding : 1em;
            z-index:9999;
        }

        #pickit-modal .panel-confirmation{
            position: fixed;
            left: 50%;
            top: 50%;
            transform: translate(-50%,-50%);
            background-color: #fff;
            border-radius: 5px;
            border: solid 1px rgba(0,0,0,.3);
            max-height: 100vh;
            height: 300px;
            overflow: hidden;
            max-width: 100vw;
            width: 800px;
            box-shadow: 1em 1em 1em rgba(0,0,0,0.2);
            padding : 1em;
            z-index:10000;
            text-align : center;
        }

        #pickit-modal .panel-confirmation h3{
            font-size: 40px;
        }

        #pickit-modal .panel-confirmation > *{
            margin : 1em;
        }

        #pickit-modal h1{
            text-align : center;
            heigth:30em;
            margin: 1em;
        }
        
        #pickit-modal .pickit-modal-content {
            display:flex;
            height: calc(100% - 9em);
        }

        #pickit-modal .pickit-modal-content > .pickit-modal-points{
            width : 50%;
            overflow : auto;
            border: solid 1px rgba(0,0,0,0.4);
        }

        #pickit-modal .pickit-modal-points li {
            border-bottom : solid 1px rgba(0,0,0,0.4);
            padding:1em;
            cursor : pointer;
            transition : background-color .5s;
            background-color : #FFF;
        }
        #pickit-modal .pickit-modal-points li:hover{
            background-color : #CCC;
        }

        #pickit-modal .close-button{
            position: absolute;
            right:1em;
            top:1em;
            font-size:30px;
            font-weight: bold;
            cursor:pointer;
            color : #ccc;
        }

        #pickit-modal .pickit-modal-points li .pickit-point-name{
            font-size:2em;
            color:chocolate;
        }

        #pickit-modal .pickit-modal-content > .pickit-modal-map{
            width:400px;
            height:300px;
            margin:auto;
        }`

        let pickitPluginLoaded = false
        let modalElement
        let map
        let pointPropmise

        function transformData(data){
            if(!data.Status || data.Status.Code != 200){
                throw new Error(data.Status.Text)
            }
            return data.Response
        }

        function getPointsPromise(){
            if(pointPropmise) return pointPropmise
            pointPropmise = 
                fetch(pickitServiceUrl,pickitServiceOptions)
                    .then(r=>r.json())
                    .then(transformData)
                    .catch(err=>{console.log(err);throw new Error("PICKIT PLUGIN ERROR: " + err)})
            return pointPropmise
        }

        function savePoint(point){
            vtexjs.checkout.getOrderForm()
                .then(function(orderForm) {
                    vtexjs.checkout.sendAttachment('openTextField', { value: "PuntoPickitID:" + point.puntoBaseId });
                })
            
        }

        function pointSelcted(point){
            const confirmation = el("div",{"class":"panel-confirmation"},[
                el("h3",{},"¿ Quiere elegir este punto ?"),
                el("div",{},point.nombre),
                el("div",{},point.direccion),
                el("button",{"class":"btn btn-success","@click":()=>{
                    savePoint(point)
                    modalElement.removeChild(confirmation)
                    closeModal()
                }},"Aceptar"),
                el("button",{"class":"btn btn-danger","@click":()=>{
                    modalElement.removeChild(confirmation)
                }},"Cancelar")
            ])
            modalElement.appendChild(confirmation)
        }

        async function createMap(elm){
            debugger
            map =  new google.maps.Map(elm,initMapOpts)
            const points = await getPointsPromise()
            for(let point of points){
                const marker = new google.maps.Marker({
                    position : {
                        lat : parseFloat(point.latitud),
                        lng : parseFloat(point.longitud)
                    },
                    icon : iconimg,
                    map
                })
                marker.addListener('click',()=>pointSelcted(point,marker))
            }
        }

        function el(type,attrs,...childs){
            const el = document.createElement(type)
            if(attrs)
                for( let attr of Object.keys(attrs) ){
                    if(attr.startsWith("@"))
                        el.addEventListener(attr.substr(1),attrs[attr])
                    else
                        el.setAttribute(attr,attrs[attr])
                }
            if(childs && childs.length){
                if(Array.isArray(childs[0]))
                    childs = childs[0]
                childs.map( c => typeof c === 'string' || c instanceof String ? document.createTextNode(c) : c )
                        .forEach( c => el.appendChild(c) )
            }
            return el
        }

        async function createPopUp(){
            if(modalElement) return
            document.head.appendChild(el("style",{"type":"text/css"},style))
            modalElement = el("div",{"id":"pickit-modal","class":"open"})
            document.body.appendChild(modalElement)
            setTimeout(async function (){
                const map = el("div",{"class":"pickit-modal-map"})
                const points = await getPointsPromise()
                const lis = points.map( p => 
                   el("li",{"@click":()=>pointSelcted(p)},[
                        el("div",{"class":"pickit-point-name"},p.nombre),
                        el("div",{"class":"pickit-point-dir"},p.direccion),
                        el("div",{"class":"pickit-point-place"},p.provinciaNombre)
                    ])
                ) 
                const pointsList = el("ul",{"class":"pickit-modal-points"},lis)
                const content = el("div",{"class":"pickit-modal-content"},[
                    pointsList,map
                ])
                const close = el("span",{
                        "class":"close-button",
                        "@click":closeModal
                    },"×")
                const panel = el("div",{"class":"panel"},[
                    close,
                    el("h1",{},"Seleccione un punto pickit"),
                    content
                ])
                createMap(map)
                modalElement.appendChild(panel)
            },0)
            return
        }

        async function showModal(){
            await createPopUp()
            modalElement.classList.add("open")
        }

        function closeModal(){
            if(modalElement)
                modalElement.classList.remove("open")
        }

        function isSlaPickit(sla){
            return sla && sla.indexOf("Pickit") > 0
        }

        function selectedSlaUpdate(){
            const li = vtexjs.checkout.orderForm.shippingData.logisticsInfo
            const sla = li && li.length && li[0].selectedSla
            if(isSlaPickit(sla)){
                showModal()
            }
        }
        
        if(pickitPluginLoaded) return
        $(window).on("deliverySelected.vtex",function (e){
            selectedSlaUpdate()
        })
        pickitPluginLoaded = true
            
    }
})(window)
