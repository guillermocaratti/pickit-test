var vtexcarriers = {
        init: function (carrierId) {
            this.carrierId = carrierId, this.carrier = "", this.carrierId.toLowerCase().indexOf("pickit") >= 0 ? this.carrier = "PICKIT" : this.carrier = this.carrierId.toLowerCase().replace("-branch-withdrawal", "").toUpperCase(), loadHTML.init(), branches.init()
        }
    },
    loadHTML = {
        init: function () {
            if (0 == $("#vtexcarriers").length) {
                var vtex_carriers_html = '<div id="vtexcarriers"><div class="vtexcarriers-wrap"><div class="wrap"><span class="close-vtex-carriers"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-times fa-stack-1x fa-inverse"></i></span></span><div class="map-wrap"><div class="left"><fieldset class="buscador"><div class="container"><label for="state">Provincia</label><span class="selected"><select name="state" id="state" class="state"></select></span></div><div class="container"><label for="city">Localidad / Barrio</label><span class="selected"><select name="city" id="city" class="city"></select></span></div><div class="mobile"><div class="container"><label for="storeLocatorSelectMobile">Seleccione la tienda</label><span class="selected"><select name="storeLocatorSelectMobile" id="storeLocatorSelectMobile" class="storeLocatorSelectMobile"></select></span></div></div></fieldset><div class="lista desktop"><h4>Resultados de búsqueda para: <span class="lugar">' + vtexcarriers.carrier + '</span></h4><ul class="list"></ul></div></div><div id="map"></div></div></div></div></div>';
                $(vtex_carriers_html).appendTo($("body")), $(".close-vtex-carriers").on("click", function () {
                    storelocator.clearMap(), dataLayer[0].vtexcarries = void 0, set_sla.init(!0), $(".shipping-option-item.active .icon-ok-circle").removeClass("icon-ok-circle").addClass("icon-circle-blank"), $(".shipping-option-item.active").removeClass("active"), $("#vtexcarriers").hide()
                })
            }
        }
    },
    sla_action = {
        init: function () {
            $("body").hasClass("vtexCarriers") || ($(document).on("click", ".shipping-option-item", function () {
                var sla_value = $(this).children("input").val();
                sla_value.toLowerCase().indexOf("retiro") >= 0 ? sla_action.run_vtexcarriers(sla_value) : (void 0 != dataLayer[0].vtexcarries && "" != dataLayer[0].vtexcarries && (dataLayer[0].vtexcarries = void 0), set_sla.init(!1))
            }), $(document).on("click", ".select-sla-btn", function () {
                $(".container > .loading.loading-bg").show();
                var sla = "[" + $(this).siblings("div.id").text() + "|" + vtexcarriers.carrierId + "] - Sucursal: " + $(this).siblings("div.name").find("span").text();
                dataLayer[0].vtexcarries = sla, set_sla.init(!0), $("#vtexcarriers").hide(), storelocator.clearMap(), $(".container > .loading.loading-bg").hide()
            }), $("body").addClass("vtexCarriers")), $(".shipping-option-item.active").trigger("click"), set_sla.init(!1)
        },
        validate_sla: function () {
            $(".container > .loading.loading-bg").show(), setTimeout(function () {
                void 0 != dataLayer[0].vtexcarries && "" != dataLayer[0].vtexcarries && set_sla.save_sla(!0), vtexjs.checkout.getOrderForm().then(function (orderForm) {
                    null != orderForm.openTextField ? "" == orderForm.openTextField.value && orderForm.shippingData.logisticsInfo[0].selectedSla.toLowerCase().indexOf("retiro") >= 0 ? (sla_action.run_vtexcarriers(orderForm.shippingData.logisticsInfo[0].selectedSla), window.location.hash = "#/shipping") : (dataLayer[0].vtexcarries = orderForm.openTextField.value, set_sla.save_sla(!0)) : $(".shipping-selected-sla .sla").text().toLowerCase().indexOf("retiro") >= 0 && orderForm.shippingData.logisticsInfo[0].selectedSla.toLowerCase().indexOf("retiro") >= 0 && (sla_action.run_vtexcarriers(orderForm.shippingData.logisticsInfo[0].selectedSla), window.location.hash = "#/shipping")
                }), sla_action.init()
            }, 100), $(".container > .loading.loading-bg").hide()
        },
        run_vtexcarriers: function (sla_value) {
            switch (sla_value = sla_value.toLowerCase(), $(".container > .loading.loading-bg").show(), set_sla.init(!0), setTimeout(function () {
                $(".btn-go-to-payment, #payment-data .link-box-edit").addClass("disable")
            }, 100), $("#vtexcarriers").show(), !0) {
                case sla_value.indexOf("andreani") >= 0:
                    vtexcarriers.init("ANDREANI-BRANCH-WITHDRAWAL");
                    break;
                case sla_value.indexOf("pickit") >= 0:
                    vtexcarriers.init(vtex.accountName.toUpperCase() + "-PICKIT");
                    break;
                default:
                    vtexcarriers.init(vtex.accountName.toUpperCase() + "-BRANCH-WITHDRAWAL")
            }
            dataLayer[0].vtexcarries = void 0, $(".vtexcarriers-wrap .left > .lista").height($(".vtexcarriers-wrap .left").height() - $(".vtexcarriers-wrap .buscador").height() + "px"), $(".container > .loading.loading-bg").hide()
        }
    },
    set_sla = {
        init: function (retiro) {
            if (retiro) {
                var sla = dataLayer[0].vtexcarries;
                0 == $(".tooltip-vtexcarriers").length ? $("#shipping-data .btn-go-to-payment-wrapper").prepend($("<span/>").addClass("tooltip-vtexcarriers").text("Seleccione la sucursal donde retirará el pedido")) : $(".tooltip-vtexcarriers").removeClass("success").text("Seleccione la sucursal donde retirará el pedido").show(), void 0 != sla && ($(".tooltip-vtexcarriers").addClass("success").text($(".shipping-option-item.active .shipping-option-item-name").text() + ": " + sla.split(" - Sucursal: ").pop()).show(), $(".btn-go-to-payment, #payment-data .link-box-edit").removeClass("disable"))
            } else $(".tooltip-vtexcarriers").hide(), $(".btn-go-to-payment, #payment-data .link-box-edit").removeClass("disable")
        },
        save_sla: function (set_sla_to_attachment) {
            var argument = "";
            set_sla_to_attachment && void 0 != dataLayer[0].vtexcarries && "" != dataLayer[0].vtexcarries && (argument = dataLayer[0].vtexcarries), vtexjs.checkout.sendAttachment("openTextField", {
                value: argument
            }).done(function (orderForm) {
                console.log("openTextField - value: ", orderForm.openTextField)
            })
        }
    },
    characteres = {
        remove_char: function (str) {
            for (var from = "ÃÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛãàáäâèéëêìíïîòóöôùúüûÑñÇç", to = "AAAAAEEEEIIIIOOOOUUUUaaaaaeeeeiiiioooouuuunncc", mapping = {}, i = 0, j = from.length; i < j; i++) mapping[from.charAt(i)] = to.charAt(i);
            for (var ret = [], i = 0, j = str.length; i < j; i++) {
                var c = str.charAt(i);
                mapping.hasOwnProperty(str.charAt(i)) ? ret.push(mapping[c]) : ret.push(c)
            }
            return ret.join("")
        }
    },
    branches = {
        init: function () {
            this.set_filters(this.get_stores())
        },
        get_stores: function () {
            var vtexcarriersUrl = "https://vtexcarriers.lyracons.com:8187/vtex/service/branch/find?courierId=",
                stores = JSON.parse($.ajax({
                    type: "GET",
                    url: vtexcarriersUrl + vtexcarriers.carrierId,
                    async: !1,
                    dataType: "json"
                }).responseText);
            return null == stores.error ? stores.values : (console.log(stores.error), !1)
        },
        sortSelectOptions: function (selector, skip_first) {
            var options = skip_first ? $(selector + " option:not(:first)") : $(selector + " option"),
                arr = options.map(function (_, o) {
                    return {
                        t: $(o).text(),
                        c: $(o).attr("class"),
                        v: o.value,
                        s: $(o).prop("selected")
                    }
                }).get();
            arr.sort(function (o1, o2) {
                var t1 = o1.t.toLowerCase(),
                    t2 = o2.t.toLowerCase();
                return t1 > t2 ? 1 : t1 < t2 ? -1 : 0
            }), options.each(function (i, o) {
                o.value = arr[i].v, $(o).text(arr[i].t), $(o).attr("class", arr[i].c), arr[i].s ? $(o).attr("selected", "selected").prop("selected", !0) : ($(o).removeAttr("selected"), $(o).prop("selected", !1))
            })
        },
        set_filters: function (dataInfo) {
            $.each(dataInfo, function (i, e) {
                if (void 0 != e.state) {
                    var stateClass = "state-" + characteres.remove_char(e.state.toLowerCase().replace(/\s/g, "-"));
                    if (0 == $("#state ." + stateClass).length && $("#state").append($("<option/>").text(e.state).attr("value", stateClass).addClass(stateClass)), void 0 != e.city) {
                        var cityClass = "city-" + characteres.remove_char(e.city.toLowerCase().replace(/\s/g, "-"));
                        0 == $("#city ." + cityClass).length && $("#city ").append($("<option/>").text(e.city).attr("value", cityClass).addClass(cityClass + " " + stateClass))
                    }
                }
            }), branches.sortSelectOptions("#state"), 0 == $("#city option").length ? $("#city").parent().parent().hide() : branches.sortSelectOptions("#city"), $(".buscador > .container").each(function () {
                if (0 == $(this).find("option[value=todas]").length) {
                    var title = $(this).children("label").text();
                    $(this).find("select").prepend($("<option/>").text("Todas").attr("value", "todas").addClass("todas")), $(this).find("select").prepend($("<option/>").text("Seleccione su " + title).attr({
                        disabled: "disabled",
                        selected: "selected"
                    }))
                }
            }), $(".buscador > .container").last().find("select").addClass("disabled"), $(".buscador > .container select").on("change", function () {
                var valueSelected = $(this).val();
                switch ($("#storeLocatorSelectMobile .store-item-mobile").remove(), $(this).attr("id")) {
                    case "state":
                        $(".buscador > .container #city").removeClass("disabled"), $(".buscador > .container #city option").addClass("disabled"), $(".buscador > .container #city option." + $(this).val() + ", .buscador > .container option.todas").removeClass("disabled"), $(".buscador > .container #city").val("todas"), "todas" == valueSelected ? (storelocator.ulList.find(".store-item").addClass("on"), $(".buscador > .container #city").addClass("disabled"), $(".lista > h4 .lugar").text("Todos los locales")) : (storelocator.ulList.find(".store-item.on").removeClass("on"), storelocator.ulList.find("." + valueSelected).addClass("on"), $(".lista > h4 .lugar").text(vtexcarriers.carrier + " - " + $("#state option." + valueSelected).text()));
                        break;
                    case "city":
                        "todas" == valueSelected ? (storelocator.ulList.find(".store-item.on").removeClass("on"), storelocator.ulList.find(".store-item." + $("select#state").val()).addClass("on"), $(".lista > h4 .lugar").text($("#state option." + $("#state").val()).text())) : (storelocator.ulList.find(".store-item.on").removeClass("on"), storelocator.ulList.find("." + valueSelected).addClass("on"), $(".lista > h4 .lugar").text(vtexcarriers.carrier + " - " + $("#state option." + $("#state").val()).text() + " - " + $("#city option." + valueSelected).text()))
                }
                storelocator.updateMarkers()
            }), $(".left > .lista .lugar").text(vtexcarriers.carrier), storelocator.init(dataInfo)
        }
    },
    storelocator = {
        init: function (dataInfo) {
            if (this.ulList = $(".lista > ul"), this.map, this.geocoder = null, this.markers = [], this.infowindow = new google.maps.InfoWindow, this.bounds = new google.maps.LatLngBounds, this.results = 0, this.mapInit(dataInfo, this.ulList), void 0 != dataLayer[0].vtexcarries && "" != dataLayer[0].vtexcarries) {
                var data = dataLayer[0].vtexcarries.split("- Sucursal").shift().replace("[", "").replace("]", "").replace(/\s/, "").split("|");
                $("." + data[1].toLowerCase() + "." + data[0]).length > 0 && $("." + data[1].toLowerCase() + "." + data[0]).trigger("click")
            }
        },
        loadStores: function (dataInfo) {
            $.each(dataInfo, function (i, data) {
                if (void 0 != data.state) {
                    var classes = "state-" + characteres.remove_char(data.state.toLowerCase().replace(/\s/g, "-"));
                    void 0 != data.city && (classes += " city-" + characteres.remove_char(data.city.toLowerCase().replace(/\s/g, "-")));
                    var storeHtml = "";
                    storeHtml += '<li class="store-item on ' + classes + " " + vtexcarriers.carrierId.toLowerCase() + " " + data.id + '">', storeHtml += '\t<div class="media">', storeHtml += '\t\t<div class="media-body">', storeHtml += '\t\t\t<div class="id" style="display:none;">' + data.id + "</div>", storeHtml += '\t\t\t\t<div class="name">', storeHtml += '\t\t\t\t\t<a href="javascript:void(0);"><span>' + data.name + "</span></a>", storeHtml += "\t\t\t\t</div>", storeHtml += '\t\t\t\t<div class="data address">' + data.address + "</div>", null != data.time && void 0 != data.time && (storeHtml += '\t\t\t\t<div class="data opening">' + data.time.replace("\r\n", "<br/>") + "</div>"), null != data.phones && void 0 != data.phones && (storeHtml += '\t\t\t\t<div class="data phone">Tel.: ' + data.phones + "</div>"), storeHtml += '\t\t\t\t\t<div class="select-sla-btn"><a href="javascript:;">Seleccionar</a></div>', storeHtml += '\t\t\t\t<div class="geodata" id="store-geodata-' + data.id + '" style="display: none" data-lat="' + data.coord.latitude + '" data-lng="' + data.coord.longitude + '">Geo: ' + data.coord.latitude + " " + data.coord.longitude + "</div>", storeHtml += "\t\t\t</div>", storeHtml += "\t\t</div>", storeHtml += "\t</div>", storeHtml += "</li>", storelocator.ulList.append(storeHtml)
                }
            })
        },
        markerClicks: function () {
            storelocator.ulList.find(".store-item.on").each(function (idx) {
                $(this).on("click", function () {
                    google.maps.event.trigger(storelocator.markers[idx], "click"), $(".store-item").removeClass("selected"), $(this).find(".name a").closest(".store-item").addClass("selected")
                })
            }), $("#storeLocatorSelectMobile").on("change", function () {
                google.maps.event.trigger(storelocator.markers[$(this).val()], "click")
            }), 1 == storelocator.ulList.find(".store-item.on").length && google.maps.event.trigger(storelocator.markers[0], "click")
        },
        mapInit: function (dataInfo) {
            storelocator.markers = [], storelocator.loadStores(dataInfo), storelocator.geocoder = new google.maps.Geocoder;
            var latlng = new google.maps.LatLng((-34.589693), (-58.518333)),
                mapOps = {
                    zoom: 10,
                    center: latlng,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    mapTypeControl: !1,
                    streetViewControl: !1,
                    rotateControl: !1,
                    fullscreenControl: !1
                };
            "undefined" == typeof storelocator.map && (storelocator.map = new google.maps.Map(document.getElementById("map"), mapOps)), storelocator.updateMarkers()
        },
        addMarker: function (name, location, idx) {
            var markerImgSrc;
            switch (!0) {
                case vtexcarriers.carrierId.toLowerCase().indexOf("andreani") >= 0:
                    markerImgSrc = "/arquivos/andreani-marker.png";
                    break;
                case vtexcarriers.carrierId.toLowerCase().indexOf("pickit") >= 0:
                    markerImgSrc = "/arquivos/pickit-marker.png";
                    break;
                default:
                    markerImgSrc = "/arquivos/marker.png"
            }
            var marker = new google.maps.Marker({
                position: location,
                title: name,
                icon: new google.maps.MarkerImage(markerImgSrc),
                map: storelocator.map
            });
            marker.setMap(storelocator.map), storelocator.markers.push(marker), google.maps.event.addListener(marker, "click", function () {
                storelocator.map.setZoom(14);
                var markerContent = $(".store-item.on:eq(" + idx + ")").html();
                markerContentParsed = markerContent.replace('<a href="javascript:void(0);">', "").replace("</a>", ""), storelocator.infowindow.setContent(markerContentParsed), storelocator.infowindow.open(storelocator.map, marker), storelocator.map.setCenter(marker.getPosition()), $(".store-item.on").removeClass("selected"), $(".store-item.on:eq(" + idx + ")").addClass("selected")
            })
        },
        setMapOnAll: function (map) {
            for (var i = 0; i < storelocator.markers.length; i++) storelocator.markers[i].setMap(map)
        },
        clearMarkers: function () {
            this.setMapOnAll(null)
        },
        showMarkers: function () {
            this.setMapOnAll(storelocator.map)
        },
        deleteMarkers: function () {
            this.clearMarkers(), storelocator.markers = [], storelocator.bounds = new google.maps.LatLngBounds
        },
        updateMarkers: function () {
            storelocator.deleteMarkers(), $(".store-item.on").each(function (idx) {
                var name = $(this).find(".name").children("a").text(),
                    position = new google.maps.LatLng($(this).find(".geodata").attr("data-lat"), $(this).find(".geodata").attr("data-lng"));
                storelocator.addMarker(name, position, idx), storelocator.bounds.extend(position), storelocator.results++, $("#storeLocatorSelectMobile").append($("<option/>").text(name).attr("value", idx).addClass($(this).attr("class") + " store-item-mobile").removeClass("store-item"))
            }), storelocator.map.fitBounds(storelocator.bounds), storelocator.map.panToBounds(storelocator.bounds), storelocator.ulList.find(".store-item").off("click"), storelocator.markerClicks(), 0 != $("#storeLocatorSelectMobile option.first-element").length && $("#storeLocatorSelectMobile option.first-element").remove(), 1 == storelocator.markers.length ? ($("#storeLocatorSelectMobile").val($("#storeLocatorSelectMobile option").first().val()).trigger("change"), google.maps.event.trigger(storelocator.markers[0], "click")) : $("#storeLocatorSelectMobile").prepend($("<option/>").addClass("first-element").text("Seleccione una Sucursal").attr({
                disable: "disable",
                selected: "true"
            }))
        },
        clearMap: function () {
            $("#vtexcarriers option").remove(), storelocator.ulList.find("li").remove(), storelocator.deleteMarkers()
        }
    };