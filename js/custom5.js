var isDebug = !1,
    BRANDLIVE = {
        tools: {
            getScript: function (source, callback) {
                var script = document.createElement("script"),
                    prior = document.getElementsByTagName("script")[0];
                script.async = 1, script.onload = script.onreadystatechange = function (_, isAbort) {
                    (isAbort || !script.readyState || /loaded|complete/.test(script.readyState)) && (script.onload = script.onreadystatechange = null, script = void 0, isAbort || callback && callback())
                }, script.src = source, prior.parentNode.insertBefore(script, prior)
            },
            imgSoftLoad: function () {
                for (var softLoadClass = " softLoaded", i = 0; i < document.images.length; i++) document.images[i].className.indexOf(softLoadClass) === -1 && (document.images[i].className += softLoadClass)
            }
        },
        checkout: {
            shippingPanelCheck: function () {
                function shippingPanelOpen(uglyFix) {
                    function uglyOpenShipping() {
                        isDebug && console.log("[ ShippingPanelCheck ] Applying Ugly Fix..."), jQuery("#shipping-data .step.shipping-data").hasClass("active") ? (clearInterval(uglyCheck), vtex.events.unsubscribe("ShippingPanelCheck")) : jQuery("#change-other-shipping-option").length > 0 && jQuery("#change-other-shipping-option")[0].click()
                    }
                    if (uglyFix) var uglyCheck = setInterval(uglyOpenShipping, 100);
                    else isDebug && console.log("[ ShippingPanelCheck ] Enabling Shipping Panel"), jQuery(".orderform-template").on("transitionend", function (event) {
                        event.target === event.currentTarget && (jQuery(".orderform-template").hasClass("active") && jQuery("#change-other-shipping-option").length > 0 && !jQuery("#shipping-data .step.shipping-data").hasClass("active") ? (jQuery("#change-other-shipping-option")[0].click(), isDebug && console.log("[ ShippingPanelCheck ] Panel Enabled"), vtex.events.unsubscribe("ShippingPanelCheck")) : isDebug && console.log("[ ShippingPanelCheck ] Panel not enabled, waiting... "), jQuery(this).off(event))
                    })
                }
                var stateLogger = [];
                vtex.events.subscribe("ShippingPanelCheck", function (state, reportData) {
                    if (stateLogger.push(state), isDebug && console.log("[ ShippingPanelCheck ] Section", stateLogger), "payment" === state) {
                        for (var emailAccesed = !1, i = 0; i < stateLogger.length; i++)
                            if ("email" === stateLogger[i]) {
                                emailAccesed = !0;
                                break
                            }
                        shippingPanelOpen(emailAccesed)
                    }
                })
            },
            setBuyButton: function () {
                var buyButton = jQuery("#cart-to-orderform"),
                    keepShopBtn = jQuery("#cart-choose-more-products");
                buyButton.length > 0 && buyButton.appendTo(".summary-totalizers .accordion-inner"), keepShopBtn.length > 0 && keepShopBtn.appendTo(".summary-totalizers .accordion-inner")
            }
        }
    };