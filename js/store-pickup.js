var mapApiURL = "https://maps.googleapis.com/maps/api/js",
    mapApiKEY = "?key=AIzaSyDM3r7kRxqZDJULVnBMW8lAgH9Yt1pLmTc",
    mapApiLIBS = "&libraries=places",
    mapApiFULL = mapApiURL + mapApiKEY + mapApiLIBS,
    mapIcon = "/arquivos/marker.png",
    fakeSla = "RetiroEnSucursal",
    idCode = "[SPU]",
    slaData = [],
    markers = [],
    firstView = !0,
    isDebug = !1,
    selectedStore, infoWindow, geocoder, map;
BRANDLIVE.storepickup = {
    init: function () {
        this.subscriber("SlaDataHandler", vtexjs, this.makeMarkersDataObject), this.mapActions(fakeSla)
    },
    tools: {
        objHas: function (obj, key) {
            return key.split(".").every(function (x) {
                return !("object" != typeof obj || null === obj || !x in obj) && (obj = obj[x], !0)
            })
        },
        objGet: function (obj, key) {
            return key.split(".").reduce(function (o, x) {
                return "undefined" == typeof o || null === o ? o : o[x]
            }, obj)
        }
    },
    mapActions: function (fakeSla) {
        function toggleMap() {
            jQuery("body").toggleClass("mapOpen"), jQuery("#storepickup").toggleClass("open"), isDebug && console.log("[ Map Actions ] Current Selected Store --> ", selectedStore), selectedStore && void 0 != selectedStore ? (jQuery('label.shipping-option-item[for*="' + selectedStore + '"]').click(), setTimeout(function () {
                jQuery('label.shipping-option-item[for*="' + fakeSla + '"]').addClass("active fakeActive")
            }, 200)) : jQuery('label.shipping-option-item[for*="' + fakeSla + '"]').siblings(":visible").first().click()
        }
        document.addEventListener("click", function (e) {
            e.target && jQuery(e.target).closest("label").hasClass("shipping-option-item") && (jQuery(e.target).closest("label").attr("for").indexOf(fakeSla) > -1 ? (toggleMap(), jQuery("#storepickup .wrapper").on("transitionend", function (event) {
                event.target === event.currentTarget && (google.maps.event.trigger(map, "resize"), jQuery(this).off(event))
            })) : selectedStore = void 0)
        }), jQuery(".closeMap").on("click", function (e) {
            e.preventDefault(), toggleMap()
        }), jQuery(document).on("click", ".store-select", function (e) {
            e.preventDefault(), selectedStore = jQuery(this).data("store"), toggleMap()
        })
    },
    geoLocate: function () {
        navigator.geolocation ? navigator.geolocation.getCurrentPosition(function (position) {
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            map.setZoom(15), map.setCenter(pos)
        }, function () {
            BRANDLIVE.storepickup.handleGeoLocationError(!0)
        }) : BRANDLIVE.storepickup.handleGeoLocationError(!1), google.maps.event.trigger(map, "resize")
    },
    handleGeoLocationError: function (browserHasGeolocation) {
        browserHasGeolocation ? isDebug && console.log("[ MAP ] - The Geolocation service failed.") : isDebug && console.log("[ MAP ] - Your browser doesn't support geolocation.")
    },
    subscriber: function (eventID, data, handler) {
        jQuery("#shipping-data .shipping-data").on("transitionend", function (event) {
            isDebug && console.log("[ " + eventID + " ] Adding Transition Listener"), event.target === event.currentTarget && (firstView ? (isDebug && console.log("[ " + eventID + " ] First View... Activating Next SLA"), jQuery('label.shipping-option-item[for*="' + fakeSla + '"]').siblings(":visible").first().click(), firstView = !1) : vtexjs.checkout.orderForm.shippingData.logisticsInfo[0].selectedSla.indexOf("[SPU]") > -1 && (isDebug && console.log("[ " + eventID + " ] Activating Fake SLA"), setTimeout(function () {
                jQuery('label.shipping-option-item[for*="' + fakeSla + '"]').addClass("active fakeActive")
            }, 100)))
        }), vtex.events.subscribe(eventID, function (state, reportData) {
            isDebug && console.log("[ " + eventID + " ] VTEX State -->", state, reportData), data && null != data && ("" != data ? (isDebug && console.log("[ " + eventID + " ] VTEX Data Found", data), handler(data, eventID, state)) : isDebug && console.log("[ " + eventID + " ] VTEX Data Not Found"))
        }), vtex.events.sendEvent(eventID)
    },
    makeMarkersDataObject: function (vtexObj, eventID) {
        if (isDebug && console.log("[ Markers Data ] Init"), void 0 != BRANDLIVE.storepickup.tools.objGet(vtexObj, "checkout.orderForm.shippingData.logisticsInfo") && "" != BRANDLIVE.storepickup.tools.objGet(vtexObj, "checkout.orderForm.shippingData.logisticsInfo")) {
            var tempObj = vtexObj.checkout.orderForm.shippingData.logisticsInfo[0];
            if (void 0 != BRANDLIVE.storepickup.tools.objGet(tempObj, "slas") && "" != BRANDLIVE.storepickup.tools.objGet(tempObj, "slas")) {
                vtex.events.unsubscribe(eventID);
                for (var tempData = tempObj.slas, x = 0, i = 0; i < tempData.length; i++) tempData[i].id.indexOf(idCode) !== -1 && (slaData[x] = {}, slaData[x].storeName = tempData[i].id.replace(idCode, ""), slaData[x].labelClass = tempData[i].id.replace(/\s/g, ""), slaData[x].address = tempData[i].pickupStoreInfo.address.street + " " + tempData[i].pickupStoreInfo.address.number + ", " + tempData[i].pickupStoreInfo.address.postalCode + ", " + tempData[i].pickupStoreInfo.address.city, "" != tempData[i].pickupStoreInfo.additionalInfo && (slaData[x].geocode = tempData[i].pickupStoreInfo.additionalInfo), x++);
                isDebug && console.log("[ Markers Data ] Object Ready", slaData), BRANDLIVE.storepickup.makeMarkersOnMap(slaData)
            } else isDebug && console.log("[ Markers Data ] SLAs Data not found, retrying...")
        } else isDebug && console.log("[ Markers Data ] Logistics Data not found, retrying...")
    },
    makeMarkersOnMap: function (slaData) {
        function makeSingleMarker(tempPosition, store, currentIndex) {
            var marker;
            tempPosition && (marker = new google.maps.Marker({
                position: tempPosition,
                zIndex: currentIndex,
                icon: mapIcon,
                map: map
            }), marker.content = "<h3>" + store.storeName + "</h3> <br><b>DIRECCIÓN:</b> " + store.address + ' <br><a class="store-select" href="#" data-store="' + store.labelClass + '">SELECCIONAR</a>', isDebug && console.log("[ MAP ] - Current Marker", marker), markers.push(marker), google.maps.event.addListener(marker, "click", function () {
                map.setCenter(this.getPosition()), infoWindow.setContent(this.content), infoWindow.open(map, this)
            }))
        }

        function geocoderCallback(store, currentIndex) {
            var geocoderResults = function (results, status) {
                var geoStore = store,
                    geoI = currentIndex;
                "OK" == status ? (isDebug && console.log("[ MAP ] - Geocoder Success -> " + status), makeSingleMarker(results[0].geometry.location.toJSON(), geoStore, geoI)) : isDebug && console.log("[ MAP ] - Geocoder Failed -> " + status)
            };
            return geocoderResults
        }
        for (var i = 0; i < slaData.length; i++) {
            var store = slaData[i];
            store.hasOwnProperty("geocode") ? makeSingleMarker(JSON.parse(store.geocode), store, i) : (isDebug && console.log("[ MAP ] - Trying with GeoCoding"), geocoder.geocode({
                address: store.address
            }, geocoderCallback(store, i))), tempPosition = void 0
        }
    },
    initMap: function () {
        map = new google.maps.Map(document.getElementById("map"), {
            center: {
                lat: -34.5891632,
                lng: -58.4492643
            },
            zoom: 12,
            zoomControl: !0,
            scaleControl: !1,
            rotateControl: !1,
            mapTypeControl: !1,
            streetViewControl: !1,
            fullscreenControl: !1
        }), geocoder = new google.maps.Geocoder, infoWindow = new google.maps.InfoWindow, BRANDLIVE.storepickup.geoLocate(), google.maps.event.addListenerOnce(map, "idle", function () {
            jQuery(function () {
                BRANDLIVE.storepickup.init()
            })
        });
        var input = document.getElementById("searchbox-input"),
            searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input), map.addListener("bounds_changed", function () {
            searchBox.setBounds(map.getBounds()), google.maps.event.trigger(map, "resize")
        }), searchBox.addListener("places_changed", function () {
            var places = searchBox.getPlaces();
            if (0 != places.length) {
                var bounds = new google.maps.LatLngBounds;
                places.forEach(function (place) {
                    return place.geometry ? void(place.geometry.viewport ? bounds.union(place.geometry.viewport) : bounds.extend(place.geometry.location)) : void(isDebug && console.log("[ MAP ] - Returned place contains no geometry"))
                }), map.fitBounds(bounds)
            }
        })
    }
}, BRANDLIVE.tools.getScript(mapApiFULL, BRANDLIVE.storepickup.initMap);