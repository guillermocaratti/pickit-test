function modalConfirmationHtml(point){
    return `<div class="panel-confirmation">
                <h3>¿ Quiere elegir este punto ?</h3>
                <div>${point.nombre}</div>
                <div>${point.direccion}</div>
                <button class="btn btn-success">Aceptar</button>
                <button class="btn btn-danger">Cancelar</button>
            </div>`
}

function liHtml (point) {
    return `<li>
                <div class="pickit-point-name">${point.nombre}</div>
                <div class="pickit-point-dir">${point.direccion}</div>
                <div class="pickit-point-place">${point.provinciaNombre}</div>
            </li>`
}
